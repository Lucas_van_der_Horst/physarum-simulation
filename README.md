# Physarum Simulation

Was learning Pytorch when I saw this video:  
https://www.youtube.com/watch?v=X-iSQQgOd1A&t=925s

So I spend the weekend trying to make a physarum simulation using Pytorch on the GPU.

## Result
![Example Video Result](.example_output/example.mp4)

## Usage
1. Install the requirements
2. Experiment with settings (located in `physarum.py`)
3. Run `physarum.py`
4. View the rendered video in `output/`