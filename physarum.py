import math
import torch
import kornia
from alive_progress import alive_bar
import imageio
import asyncio

# Settings
width, height = 960, 544
n_ants = 400000
spawn_radius = 270
step_size = 1
entropy_weight = 0.000001
steer_weight = 0.02
drop = 50
decay = 0.8
sense_angle = math.pi/4  # 45 degrees
sense_length = 2
spread = 4
spread_width = 3
device = torch.device('cuda:0')

n_frames = 3000
steps_per_frame = 2
fps = 50
export_filename = "output/slime_mold.mp4"


async def worker(queue, writer):
    while True:
        frame = await queue.get()
        writer.append_data(torch.clip(frame, 0, 255).cpu().type(torch.uint8).numpy())
        queue.task_done()
        del frame
        torch.cuda.empty_cache()


async def main():
    # Setup
    board = torch.zeros(height, width, device=device)

    ants_d = torch.rand(n_ants, device=device) * (2*math.pi)
    spawn_distances = torch.rand(n_ants, device=device) * spawn_radius
    ants_x = torch.cos(ants_d) * spawn_distances + width/2
    ants_y = torch.sin(ants_d) * spawn_distances + height/2
    ants_d += math.pi

    writer = imageio.get_writer(export_filename, format='mp4', mode='I', fps=fps)
    queue = asyncio.Queue()
    task = asyncio.create_task(worker(queue, writer))

    with alive_bar(n_frames*steps_per_frame, title="Simulating") as bar:
        for _ in range(n_frames):
            for _ in range(steps_per_frame):
                # 1 Sense
                # Direct the ants to the pheromones
                lsa, rsa = ants_d - sense_angle, ants_d + sense_angle
                left_dens = board[
                    ((ants_y + torch.sin(lsa) * sense_length) % (height-1)).type(torch.long),
                    ((ants_x + torch.cos(lsa) * sense_length) % (width-1)).type(torch.long)
                ]
                right_dens = board[
                    ((ants_y + torch.sin(rsa) * sense_length) % (height-1)).type(torch.long),
                    ((ants_x + torch.cos(rsa) * sense_length) % (width-1)).type(torch.long)
                ]
                delta_dens = right_dens - left_dens

                # 2 Rotate
                ants_d += delta_dens * steer_weight
                # Add a bit of random direction
                ants_d += torch.randn(n_ants, device=device) * delta_dens * entropy_weight

                # 3 Move the ants
                ants_x = (ants_x + torch.cos(ants_d) * step_size) % (width - 1)
                ants_y = (ants_y + torch.sin(ants_d) * step_size) % (height - 1)

                # 4 Deposit
                # Add pheromones to the image
                board[ants_y.type(torch.long), ants_x.type(torch.long)] += drop

                # 5 Diffuse
                board = kornia.gaussian_blur2d(board.view(1, 1, height, width), (spread_width,)*2, (spread,)*2,
                                               border_type='circular').view(height, width)
                # 6 Decay
                board = board * decay

                bar()

            # Add the new frame to the gif
            queue.put_nowait(board)

    with alive_bar(title='Finishing video conversion'):
        await queue.join()
    task.cancel()
    writer.close()


if __name__ == "__main__":
    asyncio.run(main())
